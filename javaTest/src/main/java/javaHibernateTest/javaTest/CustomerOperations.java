package javaHibernateTest.javaTest;
import javaHibernateTest.*;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class CustomerOperations {
	
	public CustomerLedger updateCustomerLedgerForPurchase(Customer customer, String type, int amount) {
		
		CustomerLedger customerLedger = new CustomerLedger();
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			
			session.beginTransaction();
			customerLedger.setCustomerId(customer);
			customerLedger.setAmount(-amount);
			customerLedger.setStatus("Open");
			customerLedger.setBalance(-amount);
			customerLedger.setType(type);
			session.save(customerLedger);
			session.save(customer);
			session.getTransaction().commit();
		} 
		
		catch(Exception e) {
			System.out.println(e);
		}	
	
		return customerLedger;	
	}
	
	
	public void updatePaymentScheduleForPurchase(CustomerLedger customerLedger, int amount) {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			for(int i=0;i<4;i++) {
				PaymentSchedules paymentSchedule = new PaymentSchedules();
				paymentSchedule = purchase(amount);
				int n=i+1;
				paymentSchedule.setNumber(n);
				paymentSchedule.setLedgerId(customerLedger);
				session.save(paymentSchedule);
			}
			session.getTransaction().commit();
		}
		catch(Exception e) {
			System.out.println(e);
		}	
	}
		
	
	public PaymentSchedules purchase(int amount) {		
			PaymentSchedules ps = new PaymentSchedules();
			ps.setStatus("Open");
			ps.setAmount(amount/4);	
			return ps;
	}

	
	public void updateCustomerLedgerForPayment(String type, Customer customer, int amount,CustomerLedger Ledger)  {
			
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			
		    CustomerLedger customerLedger = new CustomerLedger();
	   		session.beginTransaction();
			customerLedger.setCustomerId(customer);
			customerLedger.setStatus("Complete");
			customerLedger.setType(type);
 			customerLedger.setAmount(amount/4);
 			int ledgerId = Ledger.getId(); 
			payment(customer.getId(), ledgerId);
			
			session.save(customerLedger);
			session.getTransaction().commit();
			
			session.beginTransaction();
			int idTemp = customerLedger.getId();
			int balance = calculateBalance(idTemp);
			
			balance = customerLedger.getAmount() + balance;
			if(balance == 0) {
				customerLedger = settingPurchaseAsComplete(customerLedger);
			}
			customerLedger.setBalance(balance);
			session.save(customerLedger);
			session.getTransaction().commit();
		
	}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	
	
	public CustomerLedger settingPurchaseAsComplete(CustomerLedger customerLedger) {
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
		
			session.beginTransaction();
			int cId = customerLedger.getCustomerId().getId();
			Transaction transaction = null;

			String str = "Select id FROM CustomerLedger WHERE customer_id = :cId ORDER BY id ASC";
			Query query = session.createQuery(str);
			query.setParameter("cId", cId);
		
			query.setMaxResults(1);
			List result = query.list();
			int val = (int) result.get(0);
			
			str = "Update CustomerLedger set status=:complete where id=:val";
			query = session.createQuery(str);
			query.setParameter("complete", "Complete");
			query.setParameter("val", val);
			query.executeUpdate();
		}
		
		catch(Exception e) {
			System.out.println(e);
		}
		return customerLedger;	
	}
	
	
	public void payment(int customerId, int ledgerId) {
		int amount =0;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			
			Transaction transaction = null;
			String str = "FROM PaymentSchedules WHERE ledgerid = :ledgerId AND status = :status ORDER BY id ASC";
			Query query = session.createQuery(str);
			query.setParameter("ledgerId", ledgerId);
			query.setParameter("status", "Open");
				
			query.setMaxResults(1);
			List result = query.list();
			PaymentSchedules ps = (PaymentSchedules)result.get(0);
			amount =ps.getAmount();
			ps.setStatus("COMPLETE");
			session.save(ps);
			session.getTransaction().commit();
		}
		
		catch(Exception e) {
			System.out.println(e);
		}		
	}
	
	
	public int calculateBalance(int id) {
		int balance=100;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			Transaction transaction = null;
			id = id-1;
			String str = "Select balance FROM CustomerLedger WHERE id = :id";
			Query query = session.createQuery(str);
			query.setParameter("id", id);
			List result = query.list();
			balance = (int) result.get(0);
		}	
		catch(Exception e) {
			System.out.println(e);
		}
		
		return balance;
	}
	
	
	public void openTransactionCheck(Customer customer) {
		int cId = customer.getId();
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			Transaction transaction = null;
			String str = "FROM CustomerLedger WHERE customer_id = :cId AND status = :status AND type = :type";
			Query query = session.createQuery(str);
			query.setParameter("cId", cId);
			query.setParameter("status", "Open");
			query.setParameter("type", "purchase");
			List result = query.list();
			
			if(result.size()==0) {
				System.out.println("Customer doesn't have any open transaction");
			}
			else {
				System.out.println("Customer has open transactions");
			}
		}	
		catch(Exception e) {
			System.out.println(e);
		}
		
		
	}
	
	public int checkBalance(Customer customer) {
		int cId = customer.getId();
		int balance = 0;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			session.beginTransaction();
			Transaction transaction = null;
			String str = "Select balance FROM CustomerLedger WHERE customer_id = :cId Order by id desc";
			Query query = session.createQuery(str);
			query.setParameter("cId", cId);
			query.setMaxResults(1);
			
			List result = query.list();
			balance = (int) result.get(0);

		}	
		catch(Exception e) {
			System.out.println(e);
		}
		return balance;
		
	}
}
		
