package javaHibernateTest.javaTest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "PaymentSchedules")
public class PaymentSchedules {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="ledgerId")
	private CustomerLedger ledgerId;
	
	@Column(name="number")
	private int number;
	
	@Column(name="status")
	private String status;
	
	@Column(name="amount")
	private int amount;

	public PaymentSchedules() {
		
	}
	public CustomerLedger getLedgerId() {
		return ledgerId;
	}
	
	public void setLedgerId(CustomerLedger ledgerId) {
		this.ledgerId = ledgerId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}

}
