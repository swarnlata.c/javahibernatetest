package javaHibernateTest.javaTest;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class App {
    public static void main( String[] args ) {
       Customer customer=new Customer("Swarnlata","Ch","18-02-1997");
       
       CustomerLedger customerLedger = new CustomerLedger();
      
       CustomerOperations customerOperation = new CustomerOperations();
      
       String type="purchase";
       int amount = 40;
    
       if(type=="purchase") {
    	    customerLedger = customerOperation.updateCustomerLedgerForPurchase(customer, type, amount);
    		customerOperation.updatePaymentScheduleForPurchase(customerLedger, amount);
       }
   
       String type1="payment";
       if(type1=="payment") {
    	   customerOperation.updateCustomerLedgerForPayment(type1, customer, amount, customerLedger);    	   
       }
     
       String type2="payment";
       if(type2=="payment") {
    	   customerOperation.updateCustomerLedgerForPayment(type2, customer, amount, customerLedger);    	   
       }
       
       String type3="payment";
       if(type3=="payment") {
    	   customerOperation.updateCustomerLedgerForPayment(type3, customer, amount, customerLedger);    	   
       }   
       
       String type4="payment";
       if(type4=="payment") {
    	   customerOperation.updateCustomerLedgerForPayment(type4, customer, amount, customerLedger);    	   
       } 
       
       
       int bal = customerOperation.checkBalance(customer);
       System.out.println("Available balance:" +bal);
       
       customerOperation.openTransactionCheck(customer);
       
    }
}
